import React from 'react';
import Foods from './Foods.jsx';

const Menu = ({ menu }) => {

    const mealTypes = [...new Set(menu.map(food => food.mealType.title))];
    const navBar = mealTypes.map((meal, index) => <div key={index.toString()}>{meal}</div>);




    const menuDisplay = mealTypes.map(meal => {
        const foodsOfMeal = menu.filter(food => food.mealType.title === meal);    
        return( <Foods key={menu.id} data={foodsOfMeal} typeOfMeal={meal}/>)
    });
    return (
        <div className='main'>
            <nav className='navBar'>
                {navBar}
            </nav>
            <div className="completeMenu">
                {menuDisplay}
            </div>
        </div>
    );
}
export default Menu;