import React from 'react';

const Foods = ({ data, typeOfMeal }) => {
    return (
        <div className="foodsOfMeal">
            <h2>{typeOfMeal}</h2>
            <div className="foodGrid">


                {data.map(food => {


                    return (
                        <div className='foodDetail' key={food.id}>
                            <div>
                                <h4>{food.title}</h4>
                                <div>{food.subTitle}</div>
                            </div>
                            <div>
                                <img src={food.image} alt={food.title} />
                            </div>

                        </div>
                    )
                })}
            </div>

        </div>
    );
}
export default Foods;