import React from 'react';
import Header from './Components/Header.jsx';
import RestaurantBanner from './Components/RestaurantBanner.jsx';
import Menu from './Components/Menu.jsx';
import Footer from './Components/Footer.jsx';
import './Style/Style.css';

class App extends React.Component{
  state= {
    restaurantMenu: []
  };
  
  componentDidMount(){
    fetch('https://api.myjson.com/bins/1e9opf')
    .then(response => response.json())
    .then(data => this.setState({restaurantMenu : data}))
    .catch(error => console.error(error));
  }
  
  
  render(){
    console.log('state:' , this.state);
    
    return(
      <div className='container'>
        <Header/>
        <RestaurantBanner/>
        <Menu menu={this.state.restaurantMenu}/>
        <Footer/>
      </div>  
    );

  }
}

export default App;
